# processing (can change input/output path by args)
python scripts/create_cmangio_crop_lmdb.py --split=train
python scripts/create_cmangio_crop_lmdb.py --split=validation

# flip
export CHECKPOINT_DIR="/home/huye/projects/NVAE/output/checkpoint"
export NODE_RANK=0
export EXPR_ID=0
python train.py --data /home/huye/projects/NVAE/cmangio/cmangio-lmdb --root $CHECKPOINT_DIR --save $EXPR_ID --dataset cmangio \
    --num_channels_enc 24 --num_channels_dec 24 --epochs 200 --num_postprocess_cells 2 --num_preprocess_cells 2 \
    --num_latent_scales 5 --num_latent_per_group 20 --num_cell_per_cond_enc 2 --num_cell_per_cond_dec 2 \
    --num_preprocess_blocks 1 --num_postprocess_blocks 1 --weight_decay_norm 1e-1  --num_groups_per_scale 16 \
    --batch_size 4 --num_nf 2  --ada_groups --min_groups_per_scale 4 \
    --weight_decay_norm_anneal --weight_decay_norm_init 1. --use_se --res_dist \
    --fast_adamax --num_x_bits 5 --learning_rate 8e-3 --node_rank $NODE_RANK

# no flip
export CHECKPOINT_DIR="/home/huye/projects/NVAE/output/checkpoint"
export NODE_RANK=0
export IP_ADDR='127.0.0.1'
export IP_PORT='6023'
export EXPR_ID=1
export LOCAL_RANK=0
python train.py --data /home/huye/projects/NVAE/cmangio/cmangio-lmdb --root $CHECKPOINT_DIR --save $EXPR_ID --dataset cmangio \
    --num_channels_enc 24 --num_channels_dec 24 --epochs 200 --num_postprocess_cells 2 --num_preprocess_cells 2 \
    --num_latent_scales 5 --num_latent_per_group 20 --num_cell_per_cond_enc 2 --num_cell_per_cond_dec 2 \
    --num_preprocess_blocks 1 --num_postprocess_blocks 1 --weight_decay_norm 1e-1  --num_groups_per_scale 16 \
    --batch_size 4 --num_nf 2  --ada_groups --min_groups_per_scale 4 \
    --weight_decay_norm_anneal --weight_decay_norm_init 1. --use_se --res_dist \
    --fast_adamax --num_x_bits 5 --learning_rate 8e-3 --node_rank $NODE_RANK --master_address $IP_ADDR \
    --local_rank $LOCAL_RANK --master_port $IP_PORT

# evaluate
export CHECKPOINT_DIR="/home/huye/projects/NVAE/output/checkpoint"
export DATA_DIR="/home/huye/projects/NVAE/cmangio/cmangio-lmdb"
export NODE_RANK=0
export IP_PORT='6024'
export IP_ADDR='127.0.0.1'
export EXPR_ID=1
export LOCAL_RANK=0
python evaluate_latent.py --checkpoint $CHECKPOINT_DIR/eval-$EXPR_ID/checkpoint.pt --data $DATA_DIR --eval_mode=mapping \
    --local_rank $LOCAL_RANK --master_port $IP_PORT --exp_id 1


# smaller model for 1024
python create_cmangio_1024_lmdb.py --split=train
python create_cmangio_1024_lmdb.py --split=validation

export CHECKPOINT_DIR="/home/huye/projects/NVAE/output/checkpoint"
export NODE_RANK=0 
export IP_ADDR='127.0.0.1'
export IP_PORT='6022'
export EXPR_ID=3
export LOCAL_RANK=1 # which GPU
python train.py --data /home/huye/projects/NVAE/cmangio/cmangio-1024-lmdb --root $CHECKPOINT_DIR --save $EXPR_ID --dataset cmangio-1024 \
    --num_channels_enc 16 --num_channels_dec 16 --epochs 200 --num_postprocess_cells 2 --num_preprocess_cells 1 \
    --num_latent_scales 5 --num_latent_per_group 20 --num_cell_per_cond_enc 2 --num_cell_per_cond_dec 2 \
    --num_preprocess_blocks 1 --num_postprocess_blocks 1 --weight_decay_norm 1e-1  --num_groups_per_scale 8 \
    --batch_size 4 --num_nf 2  --ada_groups --min_groups_per_scale 1 \
    --weight_decay_norm_anneal --weight_decay_norm_init 1. --use_se --res_dist \
    --fast_adamax --num_x_bits 5 --learning_rate 8e-3 --node_rank $NODE_RANK --master_address $IP_ADDR \
    --local_rank $LOCAL_RANK --master_port $IP_PORT


# crop level scratched to 256x256
export CHECKPOINT_DIR="/home/huye/projects/NVAE/output/checkpoint"
export NODE_RANK=0
export IP_ADDR='127.0.0.1'
export IP_PORT='6024'
export EXPR_ID=2
export LOCAL_RANK=0
python train.py --data /home/huye/projects/NVAE/cmangio/cmangio-crop-256-lmdb --root $CHECKPOINT_DIR --save $EXPR_ID --dataset cmangio-crop-256 \
    --num_channels_enc 24 --num_channels_dec 24 --epochs 200 --num_postprocess_cells 2 --num_preprocess_cells 2 \
    --num_latent_scales 5 --num_latent_per_group 20 --num_cell_per_cond_enc 2 --num_cell_per_cond_dec 2 \
    --num_preprocess_blocks 1 --num_postprocess_blocks 1 --weight_decay_norm 1e-1  --num_groups_per_scale 16 \
    --batch_size 4 --num_nf 2  --ada_groups --min_groups_per_scale 4 \
    --weight_decay_norm_anneal --weight_decay_norm_init 1. --use_se --res_dist \
    --fast_adamax --num_x_bits 5 --learning_rate 8e-3 --node_rank $NODE_RANK --master_address $IP_ADDR \
    --local_rank $LOCAL_RANK --master_port $IP_PORT --cont_training

# evaluate crop level
export CHECKPOINT_DIR="/home/huye/projects/NVAE/output/checkpoint"
export DATA_DIR="/home/huye/projects/NVAE/cmangio/cmangio-crop-256-lmdb"
export NODE_RANK=0
export IP_PORT='6025'
export IP_ADDR='127.0.0.1'
export EXPR_ID=2
export LOCAL_RANK=0
export IMAGE_DIR="/home/huye/cmangio_image/train_crop_256"
python evaluate_latent.py --checkpoint $CHECKPOINT_DIR/eval-$EXPR_ID/checkpoint.pt --data $DATA_DIR --eval_mode=mapping \
    --local_rank $LOCAL_RANK --master_port $IP_PORT --exp_id 2 --img_folder_path $IMAGE_DIR


    