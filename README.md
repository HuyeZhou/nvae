# Modified from the The Official PyTorch Implementation of "NVAE: A Deep Hierarchical Variational Autoencoder" [(NeurIPS 2020 Spotlight Paper)](https://arxiv.org/abs/2007.03898) 

## github
https://github.com/NVlabs/NVAE

## Requirements
NVAE is built in Python 3.7 using PyTorch 1.6.0. Use the following command to install the requirements:
```
pip install -r requirements.txt
``` 

## processing and training
see run.sh
more details are available in the github page