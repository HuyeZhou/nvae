# ---------------------------------------------------------------
# Copyright (c) 2020, NVIDIA CORPORATION. All rights reserved.
#
# This work is licensed under the NVIDIA Source Code License
# for NVAE. To view a copy of this license, see the LICENSE file.
# ---------------------------------------------------------------

import argparse
import torch
import numpy as np
import lmdb
import os

from PIL import Image
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


def main(split, ffhq_img_path, ffhq_lmdb_path, num_images, num_train):
    assert split in {'train', 'validation'}

    # create target directory
    if not os.path.exists(ffhq_lmdb_path):
        os.makedirs(ffhq_lmdb_path, exist_ok=True)

    ind_path = os.path.join(ffhq_lmdb_path, 'train_test_ind.pt')
    if os.path.exists(ind_path):
        ind_dat = torch.load(ind_path)
        train_ind = ind_dat['train']
        test_ind = ind_dat['test']
    else:
        rand = np.random.permutation(num_images)
        train_ind = rand[:num_train]
        test_ind = rand[num_train:]
        torch.save({'train': train_ind, 'test': test_ind}, ind_path)

    file_ind = train_ind if split == 'train' else test_ind
    lmdb_path = os.path.join(ffhq_lmdb_path, '%s.lmdb' % split)

    # create lmdb
    env = lmdb.open(lmdb_path, map_size=1e12)
    count = 0
    with env.begin(write=True) as txn:
        for i in file_ind:
            img_path = os.path.join(ffhq_img_path, '%05d.png' % i)
            im = Image.open(img_path)
            im = np.array(im.getdata(), dtype=np.uint8).reshape(im.size[1], im.size[0], 3)

            txn.put(str(count).encode(), im)
            count += 1
            if count % 100 == 0:
                print(count)

        print('added %d items to the LMDB dataset.' % count)


if __name__ == '__main__':
    parser = argparse.ArgumentParser('cmangio crop images LMDB creator')
    # experimental results
    parser.add_argument('--ffhq_img_path', type=str, default='/home/huye/cmangio_image/train_crop_256',
                        help='location of crops')
    parser.add_argument('--ffhq_lmdb_path', type=str, default='/home/huye/projects/NVAE/cmangio/cmangio-crop-256-lmdb',
                        help='target location for storing lmdb files')
    parser.add_argument('--split', type=str, default='train',
                        help='training or validation split', choices=['train', 'validation'])
    parser.add_argument('--num_images', type=int, default=5279,
                        help='total number of images')
    parser.add_argument('--num_train', type=int, default=4751,
                        help='total number of training images')
    args = parser.parse_args()

    main(args.split, args.ffhq_img_path, args.ffhq_lmdb_path, args.num_images, args.num_train)

